import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { baiTapMovieBooking2Actions } from '../store/BT2MovieBooking/slice'

const Result = () => {
  const dispatch = useDispatch()
  const { chairBookings } = useSelector(state => state.baiTapMovieBooking2)
  return (
    <div>
      <h2 className='my-5 text-center'>Danh sách ghế bạn chọn</h2>
      <div className='d-flex gap-3 mt-3 align-items-center'>
        <div className='Chair booked'></div>
        <p className='m-0'>Ghế đã đặt</p>
      </div>
      <div className='d-flex gap-3 mt-3 align-items-center'>
        <div className='Chair booking'></div>
        <p className='m-0'>Ghế đang chọn</p>
      </div>
      <div className='d-flex gap-3 mt-3 align-items-center'>
        <div className='Chair'></div>
        <p className='m-0'>Ghế chưa đặt</p>
      </div>
      <table className='table mt-5'>
        <thead>
          <tr>
            <th>Số ghế</th>
            <th>Giá</th>
            <th>Hủy</th>
          </tr>
        </thead>
        <tbody>
          {
            chairBookings.map(chair =>
              <tr key={chair.soGhe}>
                <td>{chair.soGhe}</td>
                <td>{chair.gia}</td>
                <td className='text-danger fs-6 fw-bold' style={{cursor:'pointer'}} onClick={() => {
                  dispatch(baiTapMovieBooking2Actions.setChairBookings(chair))
                }}>X</td>
              </tr>
            )
          }
          <tr>
            <td>Tổng tiền cần thanh toán</td>
            <td>
              {
                chairBookings.reduce((total, chair) => total += chair.gia, 0)
              }
            </td>
          </tr>
        </tbody>
      </table>
      <button className='btn btn-warning' onClick={() => {
        dispatch(baiTapMovieBooking2Actions.setChairBooked(chairBookings))
      }}>Thanh toán</button>
    </div>
  )
}

export default Result