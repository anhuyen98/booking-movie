import React from 'react'
import Chair from './Chair'
import cn from 'classnames'

const ChairList = ({ data }) => {
  console.log(data)
  return (
    <div>
      {
        data.map(hangGhe => {
          return (
            <div key={hangGhe.hang}
              className='d-flex gap-3'
              style={{ justifyContent: 'center', alignItems: 'center' }}>
              <p style={{ width: 40, height: 40, lineHeight: '40px', textAlign: 'center' }} className='mb-0 mt-3'>{hangGhe.hang}</p>
              {
                hangGhe.danhSachGhe.map(ghe => {
                  return <Chair className={cn({disabled: !hangGhe.hang})} key={ghe.soGhe} ghe={ghe} />
                })
              }
            </div>
          )
        })
      }
    </div>
  )
}

export default ChairList