import React from 'react'
import data from './data.json'
import ChairList from './ChairList'
import Result from './Result'

const BT2MovieBooking = () => {
  return (
    <div className='container mt-5'>
      <h1>BT2MovieBooking (luyện)</h1>
      <div className='row'>
        <div className='col-8'>
          <div className='p-3 bg-dark text-white text-center my-5 fs-4'>SCREEN</div>
          <ChairList data={data}/>
        </div>
        <div className='col-4'>
          <Result />
        </div>
      </div>
    </div>
  )
}

export default BT2MovieBooking