import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import './style.scss'
import { baiTapMovieBooking2Actions } from '../store/BT2MovieBooking/slice'
import cn from 'classnames'

const Chair = ({ ghe, className }) => {
  const dispatch = useDispatch()
  const { chairBookings, chairBookeds } = useSelector(state => state.baiTapMovieBooking2)
  // console.log(chairBookings)
  return (
    <div className={cn('Chair mt-3 d-flex', className,
      {
        booking: chairBookings.find(chair => chair.soGhe === ghe.soGhe),
        booked: chairBookeds.find(chair => chair.soGhe === ghe.soGhe), 
      })} onClick={() => {
        dispatch(baiTapMovieBooking2Actions.setChairBookings(ghe))
      }}>{ghe.soGhe}</div>
  )
}

export default Chair