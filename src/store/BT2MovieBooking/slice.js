import { createSlice } from '@reduxjs/toolkit'
const initialState = {
    chairBookings: [],
    chairBookeds: []
}
const baiTapMovieBooking2Slice = createSlice({
    name: 'baiTapMovieBooking2',
    initialState,
    reducers: {
        setChairBookings(state, actions) {
            const index = state.chairBookings.findIndex(item => item.soGhe === actions.payload.soGhe)
            if (index !== -1) {
                state.chairBookings.splice(index, 1)
            } else {
                state.chairBookings.push(actions.payload)
            }
        },
        setChairBooked(state) {
            state.chairBookeds = [...state.chairBookeds, ...state.chairBookings]
            state.chairBookings = []
        }
    },
    extraReducers() {

    }
})

export const { reducer: baiTapMovieBooking2Reducer, actions: baiTapMovieBooking2Actions } = baiTapMovieBooking2Slice