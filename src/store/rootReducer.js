import { combineReducers } from "redux";
import { baiTapMovieBooking2Reducer } from "./BT2MovieBooking/slice";

export const rootReducer = combineReducers({
    baiTapMovieBooking2: baiTapMovieBooking2Reducer,
})